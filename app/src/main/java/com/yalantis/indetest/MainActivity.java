package com.yalantis.indetest;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.intel.inde.mp.IProgressListener;
import com.intel.inde.mp.MediaComposer;
import com.intel.inde.mp.android.AndroidMediaObjectFactory;
import com.intel.inde.mp.android.AudioFormatAndroid;
import com.intel.inde.mp.android.VideoFormatAndroid;
import com.intel.inde.mp.domain.MediaCodecInfo;
import com.intel.inde.mp.domain.Pair;
import com.intel.inde.mp.domain.Resolution;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.btn_start).setOnClickListener(clickListener);
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String fileName = Environment.getExternalStorageDirectory().getPath() + "/Download/qq.jpeg";
            //String fileName = Environment.getExternalStorageDirectory().getPath() + "/Download/qqw.mp4";
            String destName = Environment.getExternalStorageDirectory().getPath() + "/dest.jpeg"; // tried dest.mp4

            File file = new File(destName);
            if (file.exists()) {
                file.delete();
            }

            try {
                AndroidMediaObjectFactory factory = new AndroidMediaObjectFactory(getApplicationContext());

                JpegSubstituteEffect effect = new JpegSubstituteEffect(fileName,
                        new Resolution(500, 500), 90, factory.getEglUtil());

                //SepiaEffect effect = new SepiaEffect(90, factory.getEglUtil());

                MediaComposer mediaComposer = new MediaComposer(factory, progressListener);
                mediaComposer.addSourceFile(fileName);
                mediaComposer.setTargetFile(destName);

                // configure video encoder
                VideoFormatAndroid videoFormat = new VideoFormatAndroid("video/avc", 500, 500);
                videoFormat.setVideoBitRateInKBytes(5000);
                videoFormat.setVideoFrameRate(30);
                videoFormat.setVideoIFrameInterval(1);
                mediaComposer.setTargetVideoFormat(videoFormat);

                // configure audio encoder
                AudioFormatAndroid audioFormat = new AudioFormatAndroid("audio/mp4a-latm", 48000, 2);
                audioFormat.setAudioBitrateInBytes(96 * 1024);
                audioFormat.setAudioProfile(MediaCodecInfo.CodecProfileLevel.AACObjectLC);
                mediaComposer.setTargetAudioFormat(audioFormat);

                // configure effect
                effect.setSegment(new Pair<Long, Long>(0l, 0l)); // Apply to the entire stream
                mediaComposer.addVideoEffect(effect);

                mediaComposer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private IProgressListener progressListener = new IProgressListener() {

        @Override
        public void onMediaStart() {
            Log.i("inde", "onMediaStart");
        }

        @Override
        public void onMediaProgress(float v) {
            Log.i("inde", "onMediaProgress " + v);
        }

        @Override
        public void onMediaDone() {
            Log.i("inde", "onMediaDone");
        }

        @Override
        public void onMediaPause() {
            Log.i("inde", "onMediaPause");
        }

        @Override
        public void onMediaStop() {
            Log.i("inde", "onMediaStop");
        }

        @Override
        public void onError(Exception e) {
            Log.i("inde", "onError");
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
